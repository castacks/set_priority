#ifndef _CA_SETPRIORITY_H_
#define _CA_SETPRIORITY_H_

#include <sched.h>
#include <iostream>
#include <errno.h>
#include <string.h>
#include <ros/ros.h>

namespace CA { namespace realtime_tools {

int setPriority(ros::NodeHandle np);

} }

#endif
