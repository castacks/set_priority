/*
* Copyright (c) 2016 Carnegie Mellon University, Author <dimatura@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "set_priority.h"

//using namespace CA;

namespace CA { namespace realtime_tools {

int setPriority(ros::NodeHandle np) {

#ifdef __APPLE__
  ROS_ERROR_STREAM("Set priority is not implemented on OS X");
  return 0;
#else

  int sched_priority;
  //np.param<int>("sched/sched_priority", sched_priority, 0);

  if (!np.getParam("sched/sched_priority", sched_priority) || sched_priority==0) {
    return 0;
  }

  ROS_DEBUG_STREAM(np.getNamespace() << "requested priority: " << sched_priority);

  int schedPolicy = SCHED_RR;

  int priorityMax = sched_get_priority_max(schedPolicy);
  int priorityMin = sched_get_priority_min(schedPolicy);

  if (sched_priority < priorityMin || sched_priority > priorityMax) {
    ROS_ERROR_STREAM("Invalid requested scheduling priority: " << sched_priority << " Min/Max: " << priorityMin << "/" << priorityMax );
    sched_priority = priorityMin;
    ROS_ERROR_STREAM("Setting requested scheduling priority to lowest value: " << sched_priority);
  }


  struct sched_param params;
  params.sched_priority = sched_priority;

  int setschedulerFlag = sched_setscheduler(0, schedPolicy, &params);

  if (setschedulerFlag == -1) {
    ROS_ERROR("Set process priority failed");
    ROS_ERROR_STREAM(strerror( errno ));
  } else {
    ROS_INFO_STREAM(np.getNamespace() << " set to SCHED_RR priority: " << sched_getscheduler(0));
  }

  return setschedulerFlag;
#endif

}

} }
