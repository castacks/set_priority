/*
* Copyright (c) 2016 Carnegie Mellon University, Author <dimatura@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <sched.h>
#include <iostream>
#include <errno.h>
#include <string.h>

int main(int argc, char **argv)
{
#ifndef __APPLE__
  struct sched_param params;

  params.sched_priority = 1;
  std::cout << sched_getscheduler(0) << std::endl;

  int setschedulerFlag = sched_setscheduler(0, SCHED_RR, &params);

if (setschedulerFlag == -1)
{
  std::cout << strerror( errno ) << std::endl;
//printf ("Error opening file unexist.ent: %s\n",strerror(errno));
}
  std::cout << sched_getscheduler(0) << std::endl;
#endif
  return 0;
}
